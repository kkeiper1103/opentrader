# OpenTrader - An Open Source Tradewinds Look Alike

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://github.com/melonjs/es6-boilerplate/blob/master/LICENSE)

This is my attempt at teaching myself MelonJS by trying to recreate Tradewinds. What is Tradewinds?

## Prerequisites

- [NodeJS](https://nodejs.org/en/)

## Usage

- `npm run dev` to start the dev server on watch mode at `localhost:9000`.
- `npm run build` to generate a minified, production-ready build, in the `public` folder
