import * as me from 'melonjs/dist/melonjs.module.js';

import bootstrap from 'bootstrap/dist/js/bootstrap.esm';
import 'bootstrap/dist/css/bootstrap.css';
import 'index.css';

import HtmlGuiPlugin from './js/plugin/html-gui';

import TitleScreen from 'js/stage/title.js';
import PlayScreen from 'js/stage/play.js';
import TownScreen from "./js/stage/town.js";

import PlayerEntity from 'js/renderables/player.js';
import TownEntity from "./js/renderables/town.js";

import DataManifest from 'manifest.js';

import {TOWN_STATE} from "./js/defines/user-states.js";


import defines from './defines.js';

me.device.onReady(function () {

    me.utils.function.defer(me.plugin.register, this, HtmlGuiPlugin, "htmlgui");

    // initialize the display canvas once the device/browser is ready
    let width = defines.TILE_WIDTH * defines.TILES_PER_ROW,
        height = defines.TILE_HEIGHT * defines.TILES_PER_COLUMN;

    if (!me.video.init(width, height, {
        parent: document.getElementById("game"),
        scale: "auto",
        scaleMethod: "fit"
    })) {
        alert("Your browser does not support HTML5 canvas.");
        return;
    }

    // initialize the debug plugin in development mode.
    if (process.env.NODE_ENV === 'development' && location.hash.indexOf("debug") !== -1) {
        import('js/plugin/debug/debugPanel.js').then((plugin) => {
            // automatically register the debug panel
            me.utils.function.defer(me.plugin.register, this, plugin.DebugPanelPlugin, "debugPanel");
        });

    }

    // Initialize the audio.
    me.audio.init("mp3,ogg");

    // allow cross-origin for image/texture loading
    me.loader.crossOrigin = "anonymous";

    // set and load all resources.
    me.loader.setBaseURL("audio", "./data/bgm/");
    me.loader.preload(DataManifest, function() {
        // set the user defined game stages
        me.state.set(me.state.MENU, new TitleScreen());
        me.state.set(me.state.PLAY, new PlayScreen());
        me.state.set(TOWN_STATE, new TownScreen());


        me.state.transition("fade", "#000000", 250);


        // add our player entity in the entity pool
        me.pool.register("mainPlayer", PlayerEntity);
        me.pool.register("town", TownEntity);

        // Start the game.
        me.state.change(me.state.MENU);
    });
});
