export default {
    "wheat": {
        id: 1,
        name: "Wheat",
        base: 10
    },
    "vegetables": {
        id: 2,
        name: "Vegetables",
        base: 14
    },
    "meat": {
        id: 3,
        name: "Meat",
        base: 20
    },

    "wood": {
        id: 4,
        name: "Wood",
        base: 10
    },
    "marble": {
        id: 5,
        name: "Marble",
        base: 40
    }
};