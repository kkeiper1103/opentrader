import * as me from 'melonjs/dist/melonjs.module.js';

import data from '../data.js'

import Vue from 'vue/dist/vue.min'


class Control extends me.Renderable {

    /**
     *
     * @type HTMLElement
     */
    controlPanel = null;

    /**
     *
     * @type HTMLElement
     */
    htmlElement = null;

    /**
     *
     * @type Vue
     */
    vm = null;

    /**
     *
     * @param file
     * @param data
     */
    constructor(file, data) {
        super(0, 0, 0, 0);

        this.data = data;

        fetch(file).then(response => response.text()).then(html => {
            let template = document.createElement('template');
            template.innerHTML = html;

            this.htmlElement = template.content.firstElementChild;

            this.onResetEvent(0, 0, 0, 0);
        });

        this.controlPanel = document.getElementById("controls");
    }

    /**
     *
     * @param x
     * @param y
     * @param w
     * @param h
     */
    onResetEvent(x, y, w, h) {
        let self = this;
        this.vm = new Vue({
            el: this.htmlElement,

            data() {
                return self.data;
            },
        });


        if(this.vm.$el)
            this.controlPanel.appendChild(this.vm.$el);
    }

    /**
     *
     */
    onDestroyEvent() {
        this.controlPanel.removeChild(this.vm.$el);
    }
}


// Note : Jay Inheritance to be replaced with standard ES6 inheritance in melonjs 10+
class PlayScreen extends me.Stage {
    controlPanel = null;

    /**
     *  action to perform on state change
     */
    onResetEvent() {
        me.level.load("worldmap");
        me.audio.playTrack("Lotus");


        this.controlPanel = new Control("./ui/play.html", data);


        me.game.world.addChild(this.controlPanel);
    }

    onDestroyEvent() {
        me.audio.stopTrack();

        me.game.world.removeChild(this.controlPanel);
    }
}

export default PlayScreen;
