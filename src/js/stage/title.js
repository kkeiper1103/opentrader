import * as me from 'melonjs/dist/melonjs.module.js';

// Note : Jay Inheritance to be replaced with standard ES6 inheritance in melonjs 10+
class TitleScreen extends me.Stage {
    /**
     *  action to perform on state change
     */
    onResetEvent() {

        me.game.world.addChild(new me.ColorLayer("background", "#131313"));

        let text = new me.BitmapText(me.game.viewport.width / 2, me.game.viewport.height / 2, {
            font: "PressStart2P",
            text: "OpenTrader",
            textBaseLine: "middle",
            textAlign: "center",
            size: 4.0
        });

        me.game.world.addChild(text);

        let startButton = new me.plugins.htmlgui.Button(50, 50, "Start the Game Already!");

        startButton.callback = (e) => {
            me.state.change(me.state.PLAY);

            // this is to remove the button before fading so it doesn't look janky
            me.game.world.removeChild(startButton);
        };

        me.game.world.addChild(startButton);
    }
};

export default TitleScreen;
