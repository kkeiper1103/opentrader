import * as me from 'melonjs/dist/melonjs.module.js';
import Handlebars from 'handlebars/dist/handlebars.min.js'
import Vue from 'vue/dist/vue.min';

import * as bootstrap from 'bootstrap/dist/js/bootstrap.esm'

import data from '../data.js'


import resources from '../resources.js';


class TownControlPanel extends me.Renderable {
    /**
     *
     * @type HTMLElement
     */
    html = null;

    constructor(resources, inventory) {
        super(0, 0, 0, 0);


        this.controlPanel = document.getElementById('controls');
        this.resources = resources;
        this.inventory = inventory;

        this.onResetEvent();
    }

    /**
     *
     */
    onResetEvent() {
        //
        this.html = this.buildHtmlElementFragment("buy-sell-tabs-template", {
            resources: this.resources,
            inventory: this.inventory
        });


        let triggerTabList = [].slice.call(this.html.querySelectorAll('#tabs a.nav-link'))
        triggerTabList.forEach(function (triggerEl) {
            let tabTrigger = new bootstrap.Tab(triggerEl)

            triggerEl.addEventListener('click', function (event) {
                event.preventDefault()
                tabTrigger.show()
            })
        })


        this.controlPanel.appendChild(this.html);
    }

    /**
     *
     */
    onDestroyEvent() {
        this.controlPanel.removeChild(this.html);

        super.onDestroyEvent();
    }

    /**
     *
     * @param id string
     * @param data
     */
    buildHtmlElementFragment(id, data) {
        let contents = document.createElement('template');

        let template = Handlebars.compile( document.getElementById(id).innerHTML );

        contents.innerHTML = template( data );

        return contents.content.firstElementChild;
    }
}


class TownScreen extends me.Stage {

    available = [
        "grain", "meat", "lumber"
    ];

    resources = [
        { name: "Grain", cost: 50, id: "grain" },
        { name: "Meat", cost: 60, id: "meat" },
        { name: "Lumber", cost: 120, id: "lumber" },
        { name: "Iron", cost: 150, id: "iron" }
    ];

    /**
     *  action to perform on state change
     */
    onResetEvent(currentTown, player) {
        this.town = currentTown;
        this.player = player;


        me.game.world.addChild(
            new me.ColorLayer("bgc", "#8a7350", 0)
        );

        me.game.world.addChild(
            new me.BitmapText(10, 10, {
                font: "PressStart2P",
                text: `Welcome to ${this.town.alias}`
            })
        );

        let backToMap = new me.plugins.htmlgui.Button(10, 400, "Return to World");
        backToMap.callback = (e) => {
            me.state.change(me.state.PLAY);

            me.game.world.removeChild(backToMap);
        };

        me.game.world.addChild(backToMap);

        if(this.town.settings.bgm) {
            me.audio.playTrack(this.town.settings.bgm);
        }


        //
        this.#buildOptions();






        // debug adding coins
        me.input.bindKey(me.input.KEY.ENTER, "enter", true);
        this.handler = me.event.once(me.event.KEYDOWN, function (action, keyCode, edge) {
            console.log(edge);

            if (action === "enter") {
                // play something on tap / enter
                // this will unlock audio on mobile devices

                data.gold += 50;
                console.log(data);
            }
        });
    }

    /**
     *
     */
    onDestroyEvent() {
        this.#removeOptions();

        me.audio.stopTrack();


        me.input.unbindKey(me.input.KEY.ENTER);
        me.event.off(me.event.KEYDOWN, this.handler);
    }


    /**
     *
     */
    #buildOptions() {
        this.controlPanel = new TownControlPanel(this.resources, this.player.inventory);

        me.game.world.addChild(this.controlPanel);
    }

    /**
     *
     */
    #removeOptions() {
        me.game.world.removeChild(this.controlPanel);
    }
}

export default TownScreen;
