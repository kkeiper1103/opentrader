import * as me from "melonjs/dist/melonjs.module";

export default class extends me.Renderable {
    /**
     *
     * @type HTMLInputElement
     */
    $input = null;

    /**
     *
     * @type {function}
     */
    callback = null;

    constructor(x, y) {
        super(x, y, 0, 0);

        let $input = document.createElement('input');

        $input.style.position = "absolute";

        $input.style.left = `${ me.video.getParent().firstChild.offsetLeft + x }px`;
        $input.style.top = `${ me.video.getParent().firstChild.offsetTop + y }px`

        this.$input = $input;

        me.video.getParent().style.position = "relative";
        me.video.getParent().appendChild(this.$input);
    }


    onDestroyEvent() {
        super.onDestroyEvent();

        me.video.getParent().removeChild(this.$input);
    }
}