import * as me from 'melonjs/dist/melonjs.module.js';

import BaseInput from './base-input'

export default class extends BaseInput {
    constructor(x, y, label) {
        super(x, y);

        this.$input.type = "text";
        this.$input.placeholder = label;

        this.$input.classList.add("form-control");
        this.$input.style.maxWidth = "350px";
    }

    value() {
        return this.$input.value;
    }
};