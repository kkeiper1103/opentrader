import * as me from "melonjs/dist/melonjs.module";

import BaseInput from './base-input'


export default class extends BaseInput {
    field = null;
    options = [];
    type = 'radio';

    /**
     *
     * @param x
     * @param y
     * @param field
     */
    constructor(x, y, field) {
        super(x, y);

        this.field = field;

        // we need to remove the default input item that this extends
        me.video.getParent().removeChild(this.$input);

        this.rebuildHtml();
    }

    /**
     *
     * @param name
     * @param value
     */
    addOption(name, value) {
        if(typeof value === 'undefined')
            value = name;


        this.options.push({
            name: name,
            value: value
        });


        this.rebuildHtml();
    }

    /**
     *
     */
    rebuildHtml() {
        if(this.$html) {
            me.video.getParent().removeChild(this.$html);
        }

        let $html = `<div>
<p>${this.field}</p>`;

        for(let option of this.options) {
            $html +=
`<label>
    <input type="${this.type}" value="${option.value}" name="${this.field}"> ${option.name}
</label>`;
        }

        $html += `</div>`;

        let $container = document.createElement('template');
        $container.innerHTML = $html.trim();

        this.$html = $container.content.firstChild;

        me.video.getParent().appendChild(this.$html);
    }
}