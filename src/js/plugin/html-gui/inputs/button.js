import * as me from "melonjs/dist/melonjs.module";

import BaseInput from './base-input'

export default class Button extends BaseInput {
    /**
     *
     * @type {function}
     */
    callback = null;


    constructor(x, y, text) {
        super(x, y);

        this.$input.type = "button";
        this.$input.classList.add("btn", "btn-primary");
        this.$input.innerText = text;
        this.$input.innerHTML = text;
        this.$input.value = text;


        this.$input.addEventListener("click", (e) => {
            if(this.callback !== null)
                this.callback.apply(this, [e]);

            else
                console.warn("No Callback was Assigned!");
        });
    }
}