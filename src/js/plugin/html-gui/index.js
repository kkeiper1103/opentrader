import * as me from 'melonjs/dist/melonjs.module.js';


import Button from './inputs/button';
import TextBox from './inputs/textbox';
import Options from './inputs/options'



export default class HtmlGuiPlugin extends me.plugin.Base {
    constructor() {
        super();

        // todo anything
    }

    Button = Button;
    TextBox = TextBox;
    Options = Options;
}