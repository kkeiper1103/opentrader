import * as me from 'melonjs/dist/melonjs.module.js';
import Handlebars from 'handlebars/dist/handlebars.min.js';
import Vue from 'vue/dist/vue.min';

import * as bootstrap from 'bootstrap/dist/js/bootstrap.esm';




export default class MarketControlPanel extends me.Renderable {
    /**
     *
     * @type HTMLElement
     */
    html = null;

    constructor(resources, inventory) {
        super(0, 0, 0, 0);


        this.controlPanel = document.getElementById('controls');
        this.resources = resources;
        this.inventory = inventory;

        this.onResetEvent();
    }

    /**
     *
     */
    onResetEvent() {
        //
        this.html = this.buildHtmlElementFragment("buy-sell-tabs-template", {
            resources: this.resources,
            inventory: this.inventory
        });


        let triggerTabList = [].slice.call(this.html.querySelectorAll('#tabs a.nav-link'))
        triggerTabList.forEach(function (triggerEl) {
            let tabTrigger = new bootstrap.Tab(triggerEl)

            triggerEl.addEventListener('click', function (event) {
                event.preventDefault()
                tabTrigger.show()
            })
        })


        this.controlPanel.appendChild(this.html);
    }

    /**
     *
     */
    onDestroyEvent() {
        this.controlPanel.removeChild(this.html);

        super.onDestroyEvent();
    }

    /**
     *
     * @param id string
     * @param data
     */
    buildHtmlElementFragment(id, data) {
        let contents = document.createElement('template');

        let template = Handlebars.compile( document.getElementById(id).innerHTML );

        contents.innerHTML = template( data );

        return contents.content.firstElementChild;
    }
}
