import * as me from 'melonjs/dist/melonjs.module.js';
import Handlebars from 'handlebars/dist/handlebars.min.js'
import Vue from 'vue/dist/vue.min';

import * as bootstrap from 'bootstrap/dist/js/bootstrap.esm'


class BaseControlPanel extends me.Renderable {
    /**
     *
     * @type HTMLElement
     */
    html = null;


    /**
     *
     * @type HTMLElement
     */
    controlPanel = null;

    constructor() {
        super(0, 0, 0, 0);


        this.controlPanel = document.getElementById('controls');
        this.onResetEvent();
    }

    /**
     *
     */
    onResetEvent() {
        super.onResetEvent();

        this.controlPanel.appendChild(this.html);
    }

    /**
     *
     */
    onDestroyEvent() {
        this.controlPanel.removeChild(this.html);

        super.onDestroyEvent();
    }

    /**
     *
     * @param id string
     * @param data
     */
    buildHtmlElementFragment(id, data) {
        let contents = document.createElement('template');

        contents.innerHTML = document.getElementById(id).innerHTML;

        let vm = new Vue({
            el: contents.contents,

            data() {
                return data;
            }
        });

        return vm;
    }
}
