import * as me from 'melonjs/dist/melonjs.module.js';

export default class TownEntity extends me.Entity {
    constructor(x, y, settings) {
        super(x, y, settings);
        this.settings = settings;

        this.alias = settings.alias;

        this.body.ignoreGravity = true;
        this.body.collisionType = me.collision.types.WORLD_SHAPE;
        this.body.setCollisionMask(
            me.collision.types.PLAYER_OBJECT
        );
    }

    /**
     *
     * @param dt
     * @returns {boolean}
     */
    update(dt) {
        super.update(dt);



        return true;
    }

    /**
     *
     * @param renderer
     */
    draw(renderer) {
        if(this.highlight) {
            console.log("highlighting!");
        }

        return super.draw(renderer);
    }


    onActivateEvent() {

    }


    onCollision(response, other) {

        return true;
    }


    embarkPlayer() {
        me.state.change(me.state.PLAY, true);
    }
}