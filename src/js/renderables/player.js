import * as me from 'melonjs/dist/melonjs.module.js';
import {TOWN_STATE} from "../defines/user-states.js";
import TownEntity from "./town";

import Inventory from '../classes/inventory'

 // Note : Jay Inheritance to be replaced with standard ES6 inheritance in melonjs 10+
class PlayerEntity extends me.Entity {

    inventory = new Inventory();

    /**
     * constructor
     */
    constructor(x, y, settings) {
        // call the parent constructor
        super(x, y , settings);


        // reload position from changing state
        if(window.previous_player_x && window.previous_player_y) {
            console.log('setting previous location');
            this.pos.x = window.previous_player_x;
            this.pos.y = window.previous_player_y;

            delete window.previous_player_x
            delete window.previous_player_y
        }

        this.body.setMaxVelocity(1.3, 1.3);
        this.body.setFriction(1, 1);
        this.body.ignoreGravity = true;
        this.body.collisionType = me.collision.types.PLAYER_OBJECT;
        this.body.setCollisionMask(
            me.collision.types.WORLD_SHAPE |
            me.collision.types.ACTION_OBJECT |
            me.collision.types.ENEMY_OBJECT |
            me.collision.types.COLLECTABLE_OBJECT
        );

        this.defineAnimations().setCurrentAnimation("stand:down");
        this.lastAnimation = "walk:down";

        me.input.bindKey(me.input.KEY.A, "left");
        me.input.bindKey(me.input.KEY.D, "right");
        me.input.bindKey(me.input.KEY.W, "up");
        me.input.bindKey(me.input.KEY.S, "down");

        me.input.bindKey(me.input.KEY.E, "activate");


    }

    /**
     * update the entity
     */
    update(dt) {


        // change body force based on inputs
        if(me.input.isKeyPressed("left")) {
            if(!this.renderable.isCurrentAnimation("walk:left"))
                this.renderable.setCurrentAnimation("walk:left");

            this.body.force.x = -this.body.maxVel.x;
            this.lastAnimation = "walk:left";
        }
        else if(me.input.isKeyPressed('right')) {
            if(!this.renderable.isCurrentAnimation("walk:right"))
                this.renderable.setCurrentAnimation("walk:right");

            this.body.force.x = this.body.maxVel.x;
            this.lastAnimation = "walk:right";
        }
        else {
            this.body.force.x = 0;
        }



        if(me.input.isKeyPressed("up")) {
            if(!this.renderable.isCurrentAnimation("walk:up"))
                this.renderable.setCurrentAnimation("walk:up");

            this.body.force.y = -this.body.maxVel.y;
            this.lastAnimation = "walk:up";
        }
        else if(me.input.isKeyPressed("down")) {
            if(!this.renderable.isCurrentAnimation("walk:down"))
                this.renderable.setCurrentAnimation("walk:down");

            this.body.force.y = this.body.maxVel.y;
            this.lastAnimation = "walk:down";
        }
        else {
            this.body.force.y = 0;
        }

        // if standing still, set the stand animation
        if(this.body.force.x === 0 && this.body.force.y === 0) {
            switch(this.lastAnimation) {
                case "walk:down":
                    this.renderable.setCurrentAnimation("stand:down"); break;
                case "walk:up":
                    this.renderable.setCurrentAnimation("stand:up"); break;
                case "walk:left":
                    this.renderable.setCurrentAnimation("stand:left"); break;
                case "walk:right":
                    this.renderable.setCurrentAnimation("stand:right"); break;
            }
        }

        // call the parent method
        return super.update(dt);
    }

   /**
     * collision handler
     * (called when colliding with other objects)
     */
    onCollision(response, other) {
        let margin = {
            x: 0,
            y: 0
        };


        if(me.input.isKeyPressed("right")) {
            margin.x = -10;
        }
        else if(me.input.isKeyPressed("left")) {
            margin.x = 10;
        }

        if(me.input.isKeyPressed("down")) {
            margin.y = -10;
        }
        else if(me.input.isKeyPressed("up")) {
            margin.y = 10;
        }

        // if we collided with a town, enter the town
        if(response.b instanceof TownEntity) {
            window.previous_player_x = this.pos.x + margin.x;
            window.previous_player_y = this.pos.y + margin.y;

            this.travelTo(response.b);
        }

        return true;
    }

    /**
     *
     */
    defineAnimations() {
        this.renderable.addAnimation("stand:up", [0])
        this.renderable.addAnimation("walk:up", [
            0, 1, 2, 3
        ]);

        this.renderable.addAnimation("stand:down", [4])
        this.renderable.addAnimation("walk:down", [
            4, 5, 6, 7
        ])

        this.renderable.addAnimation("stand:left", [8])
        this.renderable.addAnimation("walk:left", [
            8, 9, 10, 11
        ]);

        this.renderable.addAnimation("stand:right", [12])
        this.renderable.addAnimation("walk:right", [
            12, 13, 14, 15
        ]);

        return this.renderable;
    }

    /**
     *
     * @param town
     */
    travelTo(town) {
        me.state.change(TOWN_STATE, town, this);
    }
};

export default PlayerEntity;
