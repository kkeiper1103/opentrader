
const defines = {
    TILE_WIDTH: 16,
    TILES_PER_ROW: 64,

    TILE_HEIGHT: 16,
    TILES_PER_COLUMN: 64,

    
};

Object.freeze(defines);

export default defines;