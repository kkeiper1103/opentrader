// a melonJS data manifest
// note : this is note a webpack manifest
const DataManifest = [

    /* Bitmap Text */
    {
        name: "PressStart2P",
        type:"image",
        src: "./data/fnt/PressStart2P.png"
    },
    {
        name: "PressStart2P",
        type:"binary",
        src: "./data/fnt/PressStart2P.fnt"
    },

    {
        name: "Chaerii",
        type: "image",
        src: "./data/img/Chaerii.png"
    },

    {
        name: "overworld_tileset_grass",
        type: "image",
        src: "./data/img/overworld_tileset_grass.png"
    },

    {
        name: "transparent",
        type: "image",
        src: "./data/img/transparent.png"
    },

    {
        name: "ui-elements",
        type: "image",
        src: "./data/img/UIpackSheet_transparent.png"
    },

    {
        name: "worldmap",
        type: "tmx",
        src: "./data/map/world.tmx"
    },

    {
        name: "Hero",
        type: "image",
        src: "./data/img/Hero.png"
    },

    // music goes here
    {
        name: "Lotus",
        type: "audio",
        src: ""
    },

    {
        name: "Industrious Ferret",
        type: "audio",
        src: ""
    },
];

export default DataManifest;
